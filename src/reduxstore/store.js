import {createStore} from "redux";
import demo from "./reducers"

var store = createStore(demo)
store.dispatch({
    type:"login"
})
console.log("........", store.getState())
store.dispatch({
    type:"LOGIN",
    payload:{email:"soni.kumar@gmail.com",name:"Soni kumari"}
})
console.log("........ login match", store.getState())
export default store