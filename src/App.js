import logo from './logo.svg';
import './App.css';
import Home from "./Home";
import Navbar from "./Navbar";
import Carousel from "./Carousel";
import Signup from "./Signup";
import Login from './Login';
import CakeDetails from "./CakeDetails";
import Detail from "./Detail";
import Search from './Search';
import Cart from './Cart';
import {useState} from 'react';
import {BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom';
import axios from "axios";
import { connect } from "react-redux";


function App(props) {
  if (localStorage.token && !props.user)
  {
  let apiurl="https://apibyashu.herokuapp.com/api/login"
  var token = localStorage.token
  axios({
           url: apiurl,
           method:"get",
           headers: { authtoken : token}
       }).then((response)=>{
           console.log("Response from signup api ", response.data)
           props.dispatch({
            type:"INITIALSE_USER",
            payload: response.data.data
        })
       }, (error)=>{
          console.log("api Error", error)
          // setError("Invalid Login")
       })

}
var [user, setUser] = useState({});
var [loginstatus, setLoginStatus] = useState(false);
function LoginDone (data) {
  console.log("LoginDone ", data)

  setUser(data)
  setLoginStatus(true)
}
   
  return (
    <div className="App">
      <Router>
      <Navbar loginstatus={loginstatus}/>
        <div>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/login" exact ><Login informlogin={LoginDone}/> </Route>
          <Route path="/signup" exact component={Signup}/>
          <Route path="/search" exact component={Search} />
          <Route path="/cake/:cakeid" exact component={Detail} />
          <Route exact path="/cart" component={Cart} />
          <Route path="/*">
          <Redirect to="/"></Redirect>
          </Route>
        </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
