import {useEffect, useState} from "react"
import axios from "axios"
import { Link, withRouter } from "react-router-dom";    //using for passing extra/hold props from component
import { connect } from "react-redux";

//var user = { }
function Login(props){
    console.log("Login Props ", props)

    var [error, setError] = useState();
    var [user, setUser] = useState({});

    // useEffect(()=>{ 
    //     //alert("Mounted and Updated")
    // }) //It will call on componentdidmount() and componentdidupdate()

    useEffect(()=>{ 
        //setUser({email:null, password:null})

        //alert("Mounted and Updated")
    },[]) //We want to prevent the call of componentdidupdate()

    let getEmail =(event)=>{
        setUser({...user,
            email: event.target.value})

        // setuser({email: event.target.value,
        //     password: user.password})
        //user.email = event.target.value
    }
    let getPassword = (event)=>{
        setUser({...user,
            password: event.target.value})

        // setuser({email: user.email, password: event.target.value})
        //user.password = event.target.value
    }
    let login = ()=>{
        let apiurl="https://apibyashu.herokuapp.com/api/login"
        axios({
                 url: apiurl,
                 method:"post",
                 data:user

             }).then((response)=>{
                 console.log("Response from signup api ", response.data)
                 if (response.data.token){
                     localStorage.token= response.data.token
                     localStorage.email=response.data.email
                     props.dispatch({
                         type:"LOGIN",
                         payload:response.data
                     })
                     props.informlogin(localStorage);
                     props.history.push("/")
                 }

             }, (error)=>{
                console.log("api Error", error)
                setError("Invalid Login")
             })
    }

    return(
        <div className="row">
        <div style={{width:"50%" , margin:"auto"}}>
            <div className="form-group">
                <label>Email</label>
            <input type="email" class="form-control" onChange={getEmail}></input>
                {/* {user && <label>user.email</label>} */}
            </div>
            <div className="form-group">
            <label>Password</label>
            <input type="password" class="form-control" onChange={getPassword}></input>
            {/* {user && <label>user.password</label>} */}
            </div>
            <div style={{color:"red"}}>
                {error}
            </div>
            <div style={{float:"right"}}>
                <Link to="/forgot">Forgot Password</Link>
            </div>
            <div>
                <Link to="/signup">New User? Click Here</Link>
            </div>
            <button className="btn btn-primary" onClick={login}>login</button>
        </div>
        </div>
    )
}

// export default withRouter(Login)
Login =withRouter(Login)
export default connect()(Login)

// export default connect(function (state, props) {
//     return { jio: state["jio"]}
// })(Login)