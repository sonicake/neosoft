import logo from './logo.svg';
import './App.css';
import Home from "./Home";
import Navbar from "./Navbar";
import Carousel from "./Carousel";
import Signup from "./Signup";
import Login from './Login';
import {useState} from 'react';


function App() {
    var [login,setLogin]=useState(false);
    var [show,setShow]=useState(false)
    var [com,setCom]=useState({});
    let showDetails=(data)=>{
        setShow(true)
        setCom(data)
    }
  return (
    <div className="App">
      <Navbar islogin={login} setlogin={setLogin}/>
      <Home />
      <Signup />
      <Login islogin={login} setlogin={setLogin}/>
    </div>
  );
}

export default App;
